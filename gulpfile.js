const { src, dest, task, watch, series, parallel } = require('gulp');

const del = require('del');
const options = require('./config.js');
const browsersync = require('browser-sync').create();

const tailwindcss = require('tailwindcss');
const autoprefixer = require('autoprefixer');
const postcss = require('gulp-postcss');
const concat = require('gulp-concat');
const uglify = require('gulp-terser');
const imagemin = require('gulp-imagemin');
const cleanCSS = require('gulp-clean-css');
const purgecss = require('gulp-purgecss');

const logSymbols = require('log-symbols');

const livePreview = (done) => {
  browsersync.init({
    server: {
      baseDir: options.paths.dist.base,
    },
    port: options.config.port || 8080,
  });
  done();
};

const previewReload = (done) => {
  console.log('\n\t' + logSymbols.info, 'Reloading Browser Preview.\n');
  browsersync.reload();
  done();
};

const devHTML = () => {
  return src(`${options.paths.src.base}/**/*.html`).pipe(
    dest(options.paths.dist.base)
  );
};

const devAssets = () => {
  return src(`${options.paths.src.assets}/**/*`).pipe(
    dest(options.paths.dist.assets)
  );
};

const devFonts = () => {
  return src(`${options.paths.src.css}/fonts/**/*`).pipe(
    dest(`${options.paths.dist.css}/fonts`)
  );
};

const devStyles = () => {
  return src(`${options.paths.src.css}/**/*.css`)
    .pipe(postcss([tailwindcss(options.config.tailwindjs), autoprefixer]))
    .pipe(
      concat({
        path: 'style.css',
      })
    )
    .pipe(dest(options.paths.dist.css));
};

const devScripts = () => {
  return src([
    `${options.paths.src.js}/**/*.js`,
    `${options.paths.src.js}/libs/**/*.js`,
  ])
    .pipe(
      concat({
        path: 'scripts.js',
      })
    )
    .pipe(dest(options.paths.dist.js));
};

const devClean = () => {
  console.log(
    '\n\t' + logSymbols.info,
    'Cleaning dist folder for fresh start.\n'
  );
  return del([options.paths.dist.base]);
};

const watchFiles = () => {
  watch(
    `${options.paths.src.base}/**/*.html`,
    series(devHTML, devStyles, previewReload)
  );
  watch(
    [options.config.tailwindjs, `${options.paths.src.css}/**/*.css`],
    series(devStyles, previewReload)
  );
  watch(`${options.paths.src.js}/**/*.js`, series(devScripts, previewReload));
  watch(`${options.paths.src.assets}/**/*`, series(devAssets, previewReload));
  console.log('Watching for Changes..\n');
};

const prodHTML = () => {
  return src(`${options.paths.src.base}/**/*.html`).pipe(
    dest(options.paths.build.base)
  );
};

const prodAssets = () => {
  return src(`${options.paths.src.assets}/**/*`)
    .pipe(imagemin())
    .pipe(dest(options.paths.build.assets));
};

const prodFonts = () => {
  return src(`${options.paths.src.css}/fonts/**/*`).pipe(
    dest(`${options.paths.build.css}/fonts`)
  );
};

const prodStyles = () => {
  return src(`${options.paths.src.css}/**/*.css`)
    .pipe(postcss([tailwindcss(options.config.tailwindjs), autoprefixer]))
    .pipe(
      concat({
        path: 'style.css',
      })
    )
    .pipe(
      purgecss({
        content: ['src/**/*.{html,js}'],
        defaultExtractor: (content) => {
          const broadMatches = content.match(/[^<>"'`\s]*[^<>"'`\s:]/g) || [];
          const innerMatches =
            content.match(/[^<>"'`\s.()]*[^<>"'`\s.():]/g) || [];
          return broadMatches.concat(innerMatches);
        },
      })
    )
    .pipe(
      cleanCSS({
        compatibility: 'ie8',
      })
    )
    .pipe(dest(options.paths.build.css));
};

const prodScripts = () => {
  return src(`${options.paths.src.js}/**/*.js`)
    .pipe(
      concat({
        path: 'scripts.js',
      })
    )
    .pipe(uglify())
    .pipe(dest(options.paths.build.js));
};

const prodClean = () => {
  console.log(
    '\n\t' + logSymbols.info,
    'Cleaning build folder for fresh start.\n'
  );
  return del([options.paths.build.base]);
};

const buildFinish = (done) => {
  console.log(
    '\n\t' + logSymbols.info,
    `Production build is complete. Files are located at ${options.paths.build.base}\n`
  );
  done();
};

exports.default = series(
  devClean,
  parallel(devStyles, devScripts, devAssets, devFonts, devHTML),
  livePreview,
  watchFiles
);

exports.prod = series(
  prodClean,
  parallel(prodStyles, prodScripts, prodAssets, prodFonts, prodHTML),
  buildFinish
);

exports.clean = series(devClean, prodClean);
