module.exports = {
  config: {
    tailwindjs: "tailwind.config.js",
    port: 3000,
  },
  paths: {
    root: ".",
    src: {
      base: "src",
      css: "src/css",
      js: "src/js",
      assets: "src/assets",
    },
    dist: {
      base: "dist",
      css: "dist/css",
      js: "dist/js",
      assets: "dist/assets",
    },
    build: {
      base: "build",
      css: "build/css",
      js: "build/js",
      assets: "build/assets",
    },
  },
};
