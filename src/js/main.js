const people = new Swiper('.s1', {
  direction: 'horizontal',
  slidesPerView: 1,
  loop: true,
  breakpoints: {
    1600: {
      slidesPerView: 2,
    },
  },
  navigation: {
    nextEl: '.people-button-next',
    prevEl: '.people-button-prev',
  },
});

const products = new Swiper('.s2', {
  direction: 'horizontal',
  slidesPerView: 1,
  centeredSlides: true,
  loop: true,
  spaceBetween: 20,
  speed: 500,
  breakpoints: {
    1400: {
      slidesPerView: 5,
    },
    815: {
      slidesPerView: 3,
    },
  },
  navigation: {
    nextEl: '.products-button-next',
    prevEl: '.products-button-prev',
  },
});
