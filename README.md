# Strona decathlon

## Wymagania:

- animacja parallax dla tła każdej sekcji
- karuzela do rzewijania ekspertów górskich na boki (wyświetlamy dwóch z pięciu)
- animacja przy zmianie trasy przy przełączaniu między trasą 1 i trasą 2
- karuzela w sekcji "solidny sorzęt" z płynnym powiększeniem środkowego zdjęcie
- hamburger i menu przypięte na stałe do prawej górnej krawędzi ekranu
- smooth scrool i przewijanie do sekcji po wybraniu jej w menu
